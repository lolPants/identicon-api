const path = require('path')
const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const { cacheSeconds } = require('route-cache')
const { generateImage, hexToRGB } = require('./helpers.js')

const app = express()
app.use(morgan('combined'))
app.use(cors())

app.get('/', cacheSeconds(60), (req, res) => {
  res.sendFile(path.join(__dirname, 'index.txt'))
})

app.get('/icon/:string\.:ext?', cacheSeconds(500), async (req, res) => { // eslint-disable-line
  let { string } = req.params
  let { circle, color, small } = req.query

  let options = {
    size: small !== undefined ? 256 : 420,
    margin: circle !== undefined ? 0.15 : 0.08,
  }

  try {
    if (color !== undefined) {
      let { r, g, b } = hexToRGB(color)
      options.foreground = [r, g, b, 255]
    }
  } catch (err) {
    // Fail silently
  }

  let file = await generateImage(string, options)
  res.set('Content-type', 'image/png')
  res.send(file)
})

app.get('/transparent/:string\.:ext?', cacheSeconds(500), async (req, res) => { // eslint-disable-line
  let { string } = req.params
  let { circle, color, small } = req.query

  let options = {
    size: small !== undefined ? 256 : 420,
    background: [0, 0, 0, 0],
    margin: circle !== undefined ? 0.15 : 0.08,
  }

  try {
    if (color !== undefined) {
      let { r, g, b } = hexToRGB(color)
      options.foreground = [r, g, b, 255]
    }
  } catch (err) {
    // Fail silently
  }

  let file = await generateImage(string, options)
  res.set('Content-type', 'image/png')
  res.send(file)
})

app.listen(process.env.PORT || 3000)
