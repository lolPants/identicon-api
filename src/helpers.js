const crypto = require('crypto')
const Identicon = require('identicon.js')

/**
 * Asynchronously generate a hash from a String
 * @param {string} string String to Hash
 * @returns {Promise.<string>}
 */
const generateHash = string => new Promise(resolve => {
  let hash = crypto
    .createHash('sha1')
    .update(string)
    .digest('hex')
  resolve(hash)
})

/**
 * Asynchronously generate an identicon image
 * @param {string} string Input String
 * @param {*} options Identicon Images
 * @returns {Promise.<Buffer>}
 */
const generateImage = (string, options) => new Promise(async resolve => {
  let hash = await generateHash(string)
  let data = new Identicon(hash, options)

  let file = Buffer.from(data.render().getBase64(), 'base64')
  resolve(file)
})

/**
 * @typedef {Object} RGB
 * @property {number} r
 * @property {number} g
 * @property {number} b
 */

/**
 * Converts a Hex Colour to RGB triplet
 * @param {string} hex Hex Color String
 * @returns {RGB}
 */
const hexToRGB = hex => {
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i
  hex = hex.replace(shorthandRegex, (m, r, g, b) => r + r + g + g + b + b)

  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
  } : null
}

module.exports = { generateHash, generateImage, hexToRGB }
